from flask import Blueprint, abort
from flask import render_template
from flask import request, flash, redirect, url_for
from flask_login import login_user, logout_user, login_required, current_user
from . import login_manager
from .forms import LoginForm, RegisterForm, TaskForm
from .models import User, Task
from .const import *
from .email import welcome_mail
page = Blueprint('page', __name__)


@login_manager.user_loader
def load_user(id):
    return User.get_by_id(id)


@page.app_errorhandler(404)
def page_not_found(error):
    # manejo de error 404
    return render_template('errors/404.html'), 404


@page.route('/')
def index():
    return render_template('index.html', title="Proyecto")


@page.route('/logout')
def logout():
    logout_user()
    flash(LOGOUT)
    return redirect(url_for('.login'))


@page.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('.tasks'))
    # instancia de forms
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User.get_by_username(form.username.data)
        if user and user.verify_password(form.password.data):
            login_user(user)
            flash(LOGIN)
            return redirect(url_for('.tasks'))
        else:
            flash(ERROR_USER_PASSWORD, 'error')

    return render_template('auth/login.html', title="Login", form=form)


@page.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('.tasks'))
    form = RegisterForm(request.form)
    if request.method == 'POST':
        if form.validate():
            user = User.create_element(form.username.data,
                                       form.password.data, form.email.data)
            if user:
                welcome_mail(user)
                form.username.data = ""
                form.password.data = ""
                form.email.data = ""
                
                flash(USER_CREATED)
            # print('usuario creado de forma exitosa \n {} \n ID: {}'.format(
            #   user.username, user.id))
    return render_template('auth/register.html', title='Registro', form=form)


@page.route('/tasks')
@page.route('/tasks/<int:page>')
@login_required
def tasks(page=1, per_page=2):
    pagination = current_user.task.paginate(page, per_page=per_page)
    tasks = pagination.items
    return render_template("task/list.html", title='Tareas',
                           tasks=tasks,
                           pagination=pagination,
                           page=page)


@page.route('/tasks/new', methods=['GET', 'POST'])
@login_required
def new_task():
    form = TaskForm(request.form)
    if request.method == 'POST' and form.validate():

        task = Task.create_element(form.title.data,
                                   form.description.data,
                                   current_user.id)
        if task:
            form.title.data = " "
            form.description.data = " "
            flash(TASK_CREATED)

    return render_template('./task/new.html', form=form, title="Nueva Tarea")


@page.route('/tasks/<int:task_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_task(task_id):
    task = Task.query.get_or_404(task_id)
    if task.user_id != current_user.id:
        # valida que tarea pertenezca a usuario logeado
        abort(404)
    form = TaskForm(request.form, obj=task)
    if request.method == 'POST' and form.validate():
        task = Task.update_element(
            task_id, form.title.data, form.description.data)
        if task:
            flash(TASK_UPDATED)

    return render_template('./task/edit.html', title="Edita tarea", form=form)


@page.route('/task/<int:task_id>/delete')
@login_required
def delete_task(task_id):
    task = Task.query.get_or_404(task_id)
    if task.user_id != current_user.id:
        # valida que tarea pertenezca a usuario logeado
        abort(404)
    if Task.delete_element(task.id):
        flash(TASK_DELETE)
    return redirect(url_for('.tasks'))

@page.route('/task/<int:task_id>/show')
@login_required
def show_task(task_id):
    task = Task.query.get_or_404(task_id)
    if task.user_id != current_user.id:
        # valida que tarea pertenezca a usuario logeado
        abort(404)
    return render_template('/task/task_info.html', title="Tarea", task=task)