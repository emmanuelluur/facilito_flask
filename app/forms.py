from wtforms.fields.html5 import EmailField
from wtforms import validators
from wtforms import Form
from wtforms import StringField, PasswordField, BooleanField, HiddenField, TextAreaField
from .models import User

def masg_validator(form, field):
    """
        Ningún usuario podra registrarse
        con el username MASG
    """
    if field.data == 'MASG' or field.data == 'masg':
        raise validators.ValidationError('El username no puede ser {}'.format(field.data))

def length_honeypot(form, field):
    if len(field.data) > 0:
        raise validators.ValidationError('Solo los humanos pueden completar el registro!')

class LoginForm(Form):
    username = StringField('Username', [
        validators.length(
            min=4, max=50, message="Username esta fuera de rango"),
    ])
    password = PasswordField('Password', [
        validators.Required(message="Contraseña requerida")
    ])


class RegisterForm(Form):
    username = StringField('Username', [
        validators.length(min=4, max=50),
        masg_validator
    ])
    email = EmailField('Correo Electronico', [
        validators.length(min=6, max=100),
        validators.Required(message="Email requerido"),
        validators.Email(message="Ingrese email valido")
    ])
    password = PasswordField('Password', [
        validators.Required(message="Contraseña requerida"),
        # valida password y confirm
        validators.EqualTo('confirm_password',
                           message="La contraseña no coincide")
    ])
    confirm_password = PasswordField('Repite Password', [
        validators.Required(message="Contraseña requerida")
    ])
    accept = BooleanField('Acepto terminos y condiciones', [
        validators.DataRequired()
    ])
    # campo previene spam
    honeypot = HiddenField("", [length_honeypot] )

    # valida campos unicos
    def validate_username(self, username):
        if User.get_by_username(username.data):
            raise validators.ValidationError("Usuario {} ya registrado".format(username.data))
    
    def validate_email(self, email):
        if User.get_by_email(email.data):
            raise validators.ValidationError("Email {} ya registrado".format(email.data))
    
    def validate(self):
        if not Form.validate(self):
            return False
        if len(self.password.data) < 4:
            self.password.errors.append("La contraseña debe ser mayor a 4 caracteres")
            return False
        return True

class TaskForm(Form):
    title = StringField('Titulo', [
        validators.length(min=4, max=50, message="Titulo fuera de rango."),
        validators.DataRequired(message="Titulo es requerido")
    ])
    description = TextAreaField('Descripción',[
        validators.DataRequired(message="Descipción es requerida")
    ], render_kw={'rows':5})
    
    honeypot = HiddenField("", [length_honeypot] )